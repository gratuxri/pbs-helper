# PBS Helpers

Proxmox Backup Server doesn't save hosts per default. These are some cron-jobs and scripts to help you to backup your hosts and monitor this with check-mk

1. /boot/efi should be mounted, to make a copy of that
2. save partition table
3. save capabilities
4. save acls
5. save xattrs
6. save efi entries
7. save filesystem UUIDs to make System bootable after restore

TODO:
  include-dev from df
  /etc/fstab.d/esp like /dev/sda2 /boot/efi vfat noauto,x-systemd.automount,x-systemd.idle-timeout=60 0 0
