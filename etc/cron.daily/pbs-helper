#!/bin/bash
PBS_HELPER_ACL="bin etc home lib lib64 media mnt opt root sbin srv usr var"
PBS_HELPER_XATTR="$PBS_HELPER_ACL"
PBS_HELPER_CAPABILITIES="usr"
PBS_HELPER_EFI=1
PBS_HELPER_PARTITIONS="sda sdb"
PBS_HELPER_UUID=1
[ -e /etc/default/pbs-helper ] && . /etc/default/pbs-helper
if [ "x$PBS_HELPER_EFI" == x1 ]; then
    if [ ! -e /var/backups/efi-entries ] ; then mkdir /var/backups/efi-entries ; fi
    mk-job backup-efi-entries efibootmgr -v >/var/backups/efi-entries/`date -I`.bak |& logger -t backup-efi-entries
    mk-job backup-efi-entries-yesterday-delete rm /var/backups/efi-entries/`date -I -d "-1 day"`.bak |& logger -t backup-efi-entries-yesterday-delete
fi
if [ "x$PBS_HELPER_PARTITIONS" != x ]; then
    for part in $PBS_HELPER_PARTITIONS; do
	if [ ! -e /var/backups/partition-table ] ; then mkdir /var/backups/partition-table ; fi
    	mk-job backup-partition-table-$part sgdisk --backup=/var/backups/partition-table/$part-`date -I`.bak /dev/sda |& logger -t backup-partition-table-$part
    	mk-job backup-partition-table-$part-yesterday-delete rm /var/backups/partition-table/$part-`date -I -d "-1 day"`.bak |& logger -t backup-partition-table-$part-yesterday-delete
    done
fi
if [ "x$PBS_HELPER_UUID" == x1 ]; then
    if [ ! -e /var/backups/uuid-info ] ; then mkdir /var/backups/uuid-info ; fi
    mk-job backup-uuid-info lsblk -o+UUID >/var/backups/uuid-info/`date -I`.bak |& logger -t backup-uuid-info
    mk-job backup-uuid-info-yesterday-delete rm /var/backups/uuid-info/`date -I -d "-1 day"`.bak |& logger -t backup-uuid-info-yesterday-delete
fi

# long running jobs, probably on same files, run in parallel
if [ "x$PBS_HELPER_ACL" != x ]; then
    if [ ! -e /var/backups/acl ] ; then mkdir /var/backups/acl ; fi
    ( cd / && mk-job backup-acl /usr/bin/getfacl -s -R -- $PBS_HELPER_ACL | gzip -c >/var/backups/acl/`date -I`.bak.gz |& logger -t backup-acl ) &
fi
if [ "x$PBS_HELPER_CAPABILITIES" != x ]; then
    if [ ! -e /var/backups/capabilities ] ; then mkdir /var/backups/capabilities ; fi
    ( cd / && mk-job backup-capabilities /sbin/getcap -r $PBS_HELPER_CAPABILITIES | gzip -c >/var/backups/capabilities/`date -I`.bak |& logger -t backup-capabilities ) &
fi
if [ "x$PBS_HELPER_XATTR" != x ]; then
    if [ ! -e /var/backups/xattrs ] ; then mkdir /var/backups/xattrs ; fi
    ( cd / && mk-job backup-xattrs /usr/bin/getfattr -d -h --absolute-names -R -P -- $PBS_HELPER_XATTR | gzip -c >/var/backups/xattrs/`date -I`.bak.gz |& logger -t backup-xattrs ) &
fi
# wait till all jobs are finished
wait
if [ "x$PBS_HELPER_ACL" != x ]; then
    mk-job backup-acl-yesterday-delete rm /var/backups/acl/`date -I -d "-1 day"`.bak.gz |& logger -t backup-acl-yesterday-delete
fi
if [ "x$PBS_HELPER_CAPABILITIES" != x ]; then
    mk-job backup-capabilities-yesterday-delete rm /var/backups/capabilities/usr-`date -I -d "-1 day"`.bak |& logger -t backup-capabilities-yesterday-delete
fi
if [ "x$PBS_HELPER_XATTR" != x ]; then
    mk-job backup-xattrs-yesterday-delete rm /var/backups/xattrs/`date -I -d "-1 day"`.bak.gz |& logger -t backup-xattrs-yesterday-delete
fi

# wait till all jobs are finished
mk-job pbs-$(hostname -s)-root proxmox-backup-client backup root.pxar:/ --skip-lost-and-found $(for i in $EXCLUDED_PATHS ; do echo '--exclude '$i ; done) $(for i in $INCLUDED_MOUNTPOINTS ; do echo '--include-dev '$i ; done) --backup-id $(hostname -s)-root $PBS_BACKUP_EXTRA_ARGS |& logger -t pbs-$(hostname -s)-root
